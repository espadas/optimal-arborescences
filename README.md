# On Finding Optimal Arborescences Algorithms.

This repository contains the implementation of the original Edmond's algorithms, as well as other more eficient implementations with improvements
and ideas, namely those by Tarjan and by Camerini et al., relying on elaborated constructions and advanced data structures, namely for efficiently keeping mergeable heaps and disjoint sets.

Check Espada's dissertation for more details: https://fenix.tecnico.ulisboa.pt/cursos/meic-a/dissertacao/1691203502343427

## Requirements
 The implementations and tests were implemented in Java 8. We used the compiler javac 1.8.0 _212. 

## Usage

### Compile
Notice that the repository is organized as:
   ```
\optimal-arborescences
     \code
          \target
     \data
          \basic-test
          \basic-test-sol
     \readme
```

To compile the solution you should run the following command (inside directory ``code``):

``mvn package``

Then, inside directory ``target``you should find the ``static_edmonds-jar-with-dependencies.jar``

### Run

To run the solution you must run the following command:

``java -jar static_edmonds-jar-with-dependencies.jar -i input_file_name.txt -o output_file_name.txt``

An example of ``input_file_name.txt`` is ``02_rinkulu.txt``which is inside directory ``basic-test``. This file contains the following lines:
```
6
0 1 10
0 2 2
0 3 10
1 2 1
1 5 8
2 3 4
3 5 4
3 4 2
4 1 2
```
The first line defines the number of vertices of the graph. The following lines define the edges, with the format (v,u,w) where v and u are vertices and w is the edge weigth.
An example of ``output_file_name.txt`` is ``02_rinkulu.txt``which is inside directory ``basic-test-sol``. This file contains the following lines:
```
0 2 2
2 3 4
3 4 2
3 5 4
4 1 2
```

