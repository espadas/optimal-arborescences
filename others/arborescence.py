import networkx as nx
import sys

import time

start_time = time.time_ns()

G = nx.read_edgelist(sys.argv[1], nodetype=int, data=(('weight',float),), create_using=nx.MultiDiGraph())
print("LOAD:", (time.time_ns() - start_time)/1000000)

A = nx.minimum_spanning_arborescence(G, attr='weight', default=0,preserve_attrs=False, partition=None)
#A = nx.minimum_branching(G, attr='weight', default=0,preserve_attrs=False, partition=None)
print("ALGO:", (time.time_ns() - start_time)/1000000)

#for x in A.edges(data=True):
#    print(x[0], x[1], x[2]['weight'])
#print("DONE:", (time.time_ns() - start_time)/1000000)

