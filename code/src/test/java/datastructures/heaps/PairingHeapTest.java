package datastructures.heaps;

import datastructures.heaps.pairing.PairingHeap;
import org.junit.jupiter.api.BeforeEach;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PairingHeapTest extends AbstractHeapTest {

    private HashMap<Integer,PairingHeap<Integer>.Node> tNodeMap;

    @BeforeEach
    public void setup() {
       setupUtil();
       this.tNodeMap = new HashMap<>();
       this.heap = new PairingHeap<Integer>(Integer::compareTo, this.tNodeMap);
    }

    @Override
    public void testHeapUnion() {
        this.populateHeap();
        assertEquals(this.heap.size(), this.data.size());

        PairingHeap<Integer> other = new PairingHeap<Integer>(Integer::compareTo, this.tNodeMap);

        for (int i = 0; i < 20; i++) {
            int t = rand.nextInt();
            other.push(t);
            this.sortedData.add(t);
        }

        this.sortedData.sort(Integer::compareTo);
        this.heap.union(other);

        assertEquals(this.sortedData.size(), this.heap.size());

        for (Integer v: this.sortedData) {
            assertEquals(v, this.heap.pop());
        }
    }
}
