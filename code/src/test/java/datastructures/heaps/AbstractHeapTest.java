package datastructures.heaps;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class AbstractHeapTest {

    protected List<Integer> data;
    protected List<Integer> sortedData;
    protected Random rand;
    protected int size;
    protected Heap<Integer> heap;

    protected void setupUtil() {

        this.rand = new Random();
        this.size = 100;

        this.data = new ArrayList<>(this.size);
        this.sortedData = new ArrayList<>(this.size);

        for (int i = 0; i < this.size; i++) {
            int t = rand.nextInt();
            this.data.add(t);
            this.sortedData.add(t);
        }

        this.sortedData.sort(Integer::compareTo);
    }


    protected void populateHeap() {
        for (Integer i: data) {
            heap.push(i);
        }
    }

    @Test
    public void testBinaryHeapPush() {
        this.populateHeap();
        for (Integer v: this.sortedData) {
            assertEquals(v, this.heap.pop());
        }
        assertTrue(this.heap.isEmpty());
    }


    @Test
    public void testHeapPush() {
        this.populateHeap();
        assertEquals(this.heap.size(), this.data.size());
    }

    @Test
    public void testHeapPop() {
        this.populateHeap();
        assertEquals(this.heap.size(), this.data.size());

        for (Integer v: this.sortedData) {
            assertTrue(this.heap.hasKey(v));
            assertEquals(v, this.heap.pop());
        }
        assertTrue(this.heap.isEmpty());
    }


    @Test
    public void testHeapDelete() {

        this.populateHeap();
        int pos = rand.nextInt(this.size) - 1;
        int deletedItem = this.sortedData.get(pos);
        this.heap.delete(deletedItem);

        assertFalse(this.heap.hasKey(deletedItem));

        this.sortedData.remove(pos);

        for (Integer v: this.sortedData) {
            assertEquals(v, this.heap.pop());
        }
    }


    @Test
    public abstract void testHeapUnion();

    @Test
    public void testHeapTop(){
        this.populateHeap();
        assertEquals(this.sortedData.get(0), this.heap.top());
    }
}
