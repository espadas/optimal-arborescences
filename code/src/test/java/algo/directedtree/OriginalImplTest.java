package algo.directedtree;

import datastructures.graph.DirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import org.junit.jupiter.api.TestInstance;

import java.util.Comparator;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OriginalImplTest extends AbstractDirectedTreeTest {

    @Override
    public DirectedTree createDirectedTree(DirectedGraph graph, Comparator<WeightedEdge> comparator) {
        return new OriginalImpl(graph, comparator);
    }
}
