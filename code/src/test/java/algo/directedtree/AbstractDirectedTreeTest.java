package algo.directedtree;

import comparators.Comparators;
import datastructures.graph.DirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import io.Directory;
import io.IO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class AbstractDirectedTreeTest {

    protected Map<String, DirectedGraph> graphs;
    protected Map<String, List<WeightedEdge>> solutions;
    protected Comparator<WeightedEdge> cmp;

    @BeforeAll
    public void setup() throws IOException {

        this.graphs = new HashMap<>();
        this.solutions = new HashMap<>();
        this.cmp = Comparators.totalOrderCmp();

        File folder = new File(Directory.TEST_GRAPH);
        String[] files = folder.list();

        for (String file : files) {
            // avoid .DSTORE mac files or other hidden files
            if (file.startsWith("."))
                continue;
            System.out.println("FILE: " + file);
            DirectedGraph graph = IO.readDirectedGraph(Directory.TEST_GRAPH + file);
            List<WeightedEdge> solution = IO.readSolution(Directory.TEST_GRAPH_SOLUTION + file);
            this.graphs.put(file, graph);
            this.solutions.put(file, solution);
        }
    }

    @BeforeEach
    public abstract DirectedTree createDirectedTree(DirectedGraph graph, Comparator<WeightedEdge> comparator);

    @Test
    public void testCameriniSimple() {
        performTest("01_simple.txt");
    }

    @Test
    public void testCameriniRinkulo(){
        performTest("02_rinkulu.txt");
    }

    @Test
    public void testCameriniSimpleCycle(){
        performTest("03_simple_cycle.txt");
    }

    @Test
    public void testCameriniMultiEdge(){
        performTest("04_multi_edge.txt");
    }

    @Test
    public void testCameriniLargeCycle(){
        performTest("05_large_cycle.txt");
    }

    @Test
    public void testCameriniNonRoot(){
        performTest("06_non-1_root.txt");
    }

    @Test
    public void testCameriniTwoCycles(){
        performTest("07_two_cycles.txt");
    }

    @Test
    public void testCameriniParallelCycles(){
        performTest("08_parallel_cycles.txt");
    }

    @Test
    public void testCameriniNestedCycles(){
        performTest("09_nested_cycles.txt");
    }

    @Test
    public void testCameriniCompleteK5(){
        performTest("10_complete_graph_K5.txt");
    }

    @Test
    public void testCameriniWorstCase(){
        performTest("11_worst_case.txt");
    }

    @Test
    public void testCameriniAsymetric(){
        performTest("12_asymmetric.txt");
    }

    private void performTest(String name) {
        DirectedTree directedTree = createDirectedTree(this.graphs.get(name), this.cmp);
        util(directedTree, name);
    }

    private void util(DirectedTree edmonds, String name) {
        System.out.println("Running test: " + name);
        List<WeightedEdge> sol = edmonds.getDirectedTree();
        List<WeightedEdge> correctSol = solutions.get(name);
        System.out.println("Solution: " + sol);
        assertEquals(correctSol.size(), sol.size());
        assertEquals(edmonds.computeWeight(correctSol), edmonds.computeWeight(sol));
        assertEquals(new HashSet<>(correctSol), new HashSet<>(sol));
    }
}
