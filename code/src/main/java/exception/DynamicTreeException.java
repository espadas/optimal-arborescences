package exception;

public class DynamicTreeException extends RuntimeException {

    public DynamicTreeException(String message) {
        super(message);
    }
}
