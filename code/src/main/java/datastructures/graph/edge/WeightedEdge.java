package datastructures.graph.edge;

import java.io.Serializable;
import java.util.Objects;

public class WeightedEdge extends Edge implements Serializable {

    private static final long serialVersionUID = 1445664L;

    private Number weight;
    private Number originalWeight;
    private WeightedEdge parent;

    /**
     * Class constructor
     *
     * @param u - node u
     * @param v - node v
     * @param w - weight of the edge
     */
    public WeightedEdge(int u, int v, Number w) {
        super(u, v);
        this.weight = w;
        this.originalWeight = w;
    }

    /**
     * Class constructor used to build a copy of edge object
     *
     * @param e - WeightedEdge object
     */
    public WeightedEdge(WeightedEdge e) {
        super(e);
        this.weight = e.weight;
        this.parent = e.parent;
        this.originalWeight = e.originalWeight;
    }

    /**
     * Setter for attribute parent
     * @param edge - edge
     */
    public void setParent(WeightedEdge edge){
        this.parent = edge;
    }

    /**
     * Set original weight parameter
     * @param originalWeight - weight
     */
    public void setOriginalWeight(Number originalWeight) {
        this.originalWeight = originalWeight;
    }

    public Number getOriginalWeight() {
        return this.originalWeight;
    }

    public WeightedEdge getParent() {
        return this.parent;
    }


    /**
     * Getter for weight attribute
     *
     * @return
     */
    public Number getWeight() {
        return this.weight;
    }

    /**
     * Override of to string method
     *
     * @return string containing edge information
     */
    @Override
    public String toString() {
        return "src: " + this.src + " dest: " + this.dest + " weight: " + this.weight;
    }

    /**
     * This function updates the weight the edge
     *
     * @param w - new weight
     */
    public void setWeight(Number w) {
        this.weight = w;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        WeightedEdge that = (WeightedEdge) o;
        return Objects.equals(weight, that.weight) &&
                Objects.equals(originalWeight.floatValue(), that.originalWeight.floatValue()) &&
                Objects.equals(parent, that.parent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weight, originalWeight, parent);
    }
}
