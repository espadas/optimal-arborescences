package datastructures.graph;

import datastructures.graph.edge.Edge;
import datastructures.graph.edge.WeightedEdge;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UndirectedGraph extends BaseGraph implements GraphInterface {

    public UndirectedGraph(int v) {
        super(v);
    }

    public UndirectedGraph(BaseGraph graph) {
        super(graph);
    }

    public UndirectedGraph() {
        super();
    }

    /**
     * Returns a list of edge neighbors of src
     *
     * @param src - node
     * @return neighbors of src
     */
    @Override
    public List<WeightedEdge> getNeighbors(int src) {
        return adjList.get(src);
    }

    /**
     * Returns all edges on the graph
     *
     * @return list of all edges on the graph
     */
    @Override
    public List<WeightedEdge> getAllEdges() {
        LinkedList<WeightedEdge> edgeLinkedList = new LinkedList<>();
        Set<Integer> nodeSet = this.getNodeSet();

        for (Integer u : nodeSet) {
            edgeLinkedList.addAll(this.getNeighbors(u));
        }

        return edgeLinkedList;
    }

    /**
     * Add an edge to the graph
     *
     * @param src - source
     * @param dst - destination
     * @param w   - weight
     */
    @Override
    public void addEdge(int src, int dst, int w) {
        WeightedEdge weightedEdge = new WeightedEdge(src, dst, w);
        WeightedEdge reverseEdge = new WeightedEdge(dst, src, w);
        this.addEdgeUtil(src, dst, weightedEdge, reverseEdge);
    }

    /**
     * Add an edge to the graph
     *
     * @param edge - weighted edge
     */
    @Override
    public void addEdge(WeightedEdge edge) {
        int src = edge.getSrc();
        int dst = edge.getDest();
        Number w = edge.getWeight();
        WeightedEdge reversedEdge = new WeightedEdge(dst, src, w);
        this.addEdgeUtil(src, dst, edge, reversedEdge);
    }

    /**
     * Util function to add edge to the graph
     *
     * @param src          - edge source
     * @param dst          - edge destination
     * @param weightedEdge - edge with normal waypoints
     * @param reverseEdge  - edge with the waypoints reversed
     */
    private void addEdgeUtil(int src, int dst, WeightedEdge weightedEdge, WeightedEdge reverseEdge) {

        if (!this.adjList.containsKey(src)) {
            LinkedList<WeightedEdge> edgeLinkedList = new LinkedList<>();
            edgeLinkedList.add(weightedEdge);
            this.adjList.put(src, edgeLinkedList);
        }else if (!this.adjList.containsKey(dst)) {
            LinkedList<WeightedEdge> edgeLinkedList = new LinkedList<>();
            edgeLinkedList.add(reverseEdge);
            this.adjList.put(dst, edgeLinkedList);
        } else {
            List<WeightedEdge> edgeLinkedList = this.adjList.get(src);
            edgeLinkedList.add(weightedEdge);
            edgeLinkedList = this.adjList.get(dst);
            edgeLinkedList.add(reverseEdge);
        }
    }

    /**
     * Remove edge from graph
     * @param src - edge source
     * @param dst - edge destination
     */
    @Override
    public void removeEdge(int src, int dst) {
        List<WeightedEdge> edgeLinkedList1 = this.getNeighbors(src);
        List<WeightedEdge> edgeLinkedList2 = this.getNeighbors(dst);
        this.removeEdgeUtil(edgeLinkedList1, src, dst);
        this.removeEdgeUtil(edgeLinkedList2, dst, src);
    }

    /**
     * Util function to remove edge from graph
     * @param edgeLinkedList - neighbors
     * @param src - edge source
     * @param dst - edge destination
     */
    private void removeEdgeUtil(List<WeightedEdge> edgeLinkedList, int src, int dst) {
        int k = 0;
        for (Edge edge : edgeLinkedList) {
            if (edge.getSrc() == src && edge.getDest() == dst) {
                break;
            }
            k++;
        }
        edgeLinkedList.remove(k);
    }

    /**
     * Returns the number of nodes on the graph
     * @return number of nodes
     */
    @Override
    public int size() {
        return getNodeSet().size();
    }

    /**
     * Compute number of edges
     * @return number of edges
     */
    @Override
    public int edges() {

        int result = 0;

        Set<Integer> nodeSet = this.getNodeSet();

        for (Integer node : nodeSet) {

            result += this.getNeighbors(node).size();
        }

        return (result/2);
    }

    /**
     * Override of toString method
     *
     * @return String with all edges of the graph
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<Integer, List<WeightedEdge>> entry : this.adjList.entrySet()) {
            List<WeightedEdge> weightedEdges = entry.getValue();
            Integer node = entry.getKey();
            builder.append(node);
            builder.append(": ");
            builder.append(weightedEdges.toString());
            builder.append("\n");
        }
        return builder.toString();
    }
}
