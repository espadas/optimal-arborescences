package datastructures.heaps.pairing;

import datastructures.heaps.Heap;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class PairingHeap<T> implements Heap<T> {

    private Node head;
    private Comparator<T> cmp;
    private Map<T, Node> tNodeMap;

    public PairingHeap(Comparator<T> cmp, Map<T, Node> tNodeMap) {

        if (cmp == null) {
            throw new IllegalArgumentException("The comparator must not be null.");
        }

        this.head = null;
        this.cmp = cmp;
        this.tNodeMap = tNodeMap;
    }

    @Override
    public Comparator<T> comparator() {
        return this.cmp;
    }

    @Override
    public void push(T key) {
        if (this.keyToNode(key) != null)
            throw new IllegalStateException("The key exists in the heap.");

        Node y = new Node(key, null, null, null);
        this.tNodeMap.put(key, y);
        this.head = this.merge(this.head, y);
        this.head.parent = null;
    }

    @Override
    public void update(T key, T newKey) {

        int cmp_result = cmp.compare(key, newKey);

        if (cmp_result > 0) {
            /* Decrease key. */
            Node x = this.keyToNode(key);
            if (x == null) return;

            this.tNodeMap.remove(key);
            this.tNodeMap.put(newKey, x);
            x.key = newKey;

            if (x.parent == null || x.compareTo(x.parent) >= 0)
                return;

            x.parent.removeChild(x);
            this.head = merge(this.head, x);
            this.head.parent = null;
        } else if (cmp_result < 0) {
            /* Increase key. */
            this.delete(key);
            this.push(newKey);
        }
    }

    @Override
    public void delete(T key) {
        Node x = this.keyToNode(key);

        if (x == null)
            throw new IllegalStateException("The key is unknown.");

        if (x.parent != null)
            x.parent.removeChild(x);

        Node y = twoPassMerge(x.child);
        if (x != this.head)
            this.head = merge(this.head, y);
        else
            this.head = y;
        if (this.head != null)
            this.head.parent = null;
        this.tNodeMap.remove(key);
    }

    @Override
    public void union(Heap<T> heap) {
        this.head = merge(this.head, ((PairingHeap<T>) heap).head);
        if (this.head != null)
            this.head.parent = null;
    }

    @Override
    public T top() {
        if (this.head == null)
            return null;
        return this.head.key;
    }

    @Override
    public T pop() {
        if (this.head == null)
            throw new IllegalStateException("The heap is empty.");

        T minKey = this.head.key;
        this.head = twoPassMerge(this.head.child);
        if (this.head != null)
            this.head.parent = null;
        this.tNodeMap.remove(minKey);

        return minKey;
    }

    @Override
    public int size() {
        return this.head == null ? 0 : this.head.size();
    }

    public boolean isEmpty() {
        return this.head == null;
    }

    @Override
    public boolean hasKey(T key) {
        return this.tNodeMap.containsKey(key);
    }

    @Override
    public void setComparator(Comparator<T> cmp) {
        this.cmp = cmp;
    }

    @Override
    public Set<T> getKeySet() {
        Set<T> set = new HashSet<>();
        LinkedList<Node> ss = new LinkedList<>();

        if (this.head != null) {
            ss.push(this.head);
            while(! ss.isEmpty()) {
                Node n = ss.pop();
                set.add(n.key);

                if(n.sibling != null)
                    ss.push(n.sibling);
                if (n.child != null)
                    ss.push(n.child);
            }
        }

        return set;
    }

    public void clear() {

        this.head = null;
    }

    private Node keyToNode(T key){
        return this.tNodeMap.get(key);
    }

    public class Node {

        T key;
        Node child;
        Node sibling;
        Node parent;

        public Node() {
            this(null, null, null, null);
        }

        public Node(T key, Node child, Node sibling, Node parent) {
            this.key = key;
            this.child = child;
            this.sibling = sibling;
            this.parent = parent;
        }

        public int compareTo(Node other) {
            return cmp.compare(this.key, other.key);
        }

        public void addChild(Node node) {
            node.sibling = this.child;
            node.parent = this;
            this.child = node;
        }

        public void removeChild(Node node) {
            Node y = this.child;
            Node z = null;
            while (y != node) {
                z = y;
                y = y.sibling;
            }

            if (y == node)
                if (z != null)
                    z.sibling = node.sibling;
                else
                    this.child = node.sibling;
        }

        public int size() {
            int sz = 1;
            Node x = this.child;
            for (; x!= null; x = x.sibling)
                sz += x.size();
            return sz;
        }
    }

    private Node merge(Node x, Node y) {
        if (x == null) return y;
        if (y == null) return x;

        if (x.compareTo(y) < 0) {
            x.addChild(y);
            return x;
        } else {
            y.addChild(x);
            return y;
        }
    }

    private Node twoPassMerge(Node x) {
        if (x == null || x.sibling == null)
            return x;

        Node y = x.sibling;
        Node z = y.sibling;
        x.sibling = null;
        y.sibling = null;

        return merge(merge(x, y), twoPassMerge(z));
    }
}
