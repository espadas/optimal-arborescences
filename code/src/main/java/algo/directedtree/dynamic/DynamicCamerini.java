package algo.directedtree.dynamic;

import algo.directedtree.camerini.Camerini;
import algo.directedtree.camerini.EdgeNode;
import datastructures.disjointsets.DisjointSet;
import datastructures.disjointsets.FasterDirectedMSTDisjointSet;
import datastructures.graph.DirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import datastructures.heaps.Heap;
import datastructures.heaps.pairing.PairingHeap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DynamicCamerini extends Camerini {

    private WeightedEdge[] maxEdgeCycle;
    private Number[] maxCycleEdgeWeights;
    private List<ATNode>[] contractedEdges;

    public DynamicCamerini(DirectedGraph G, Comparator<WeightedEdge> comparator) {

        super(G, comparator);

        this.maxEdgeCycle = new WeightedEdge[this.G.size()];
        this.maxCycleEdgeWeights = new Number[this.G.size()];
        this.contractedEdges = new List[this.G.size()];
        this.cameriniForest = new ATree(this.G.size(), this.G.edges());

        List<WeightedEdge> allEdges = this.G.getAllEdges();
        for (WeightedEdge edge : allEdges) {
            this.queues[edge.getDest()].push(new ATNode(edge));
        }

    }

    public DynamicCamerini(DirectedGraph G, Comparator<WeightedEdge> comparator, ATree aTree, List<EdgeNode> aTreeRoots,
                           List<EdgeNode> removedContracted, WeightedEdge newEdge, WeightedEdge removeEdge)
    {

        this.G = new DirectedGraph(G);
        this.G.reverseGraph();

        this.cmp = comparator;

        this.maxDisjointCmp = (o1, o2) -> {
            WeightedEdge e1 = getAdjustedWeightedEdge(o1.getWeightedEdge());
            WeightedEdge e2 = getAdjustedWeightedEdge(o2.getWeightedEdge());
            return cmp.compare(e1, e2);
        };

        this.stronglyConnected = new FasterDirectedMSTDisjointSet(this.G.size());
        this.weaklyConnected = new DisjointSet(this.G.size());
        this.queues = new Heap[this.G.size()];
        this.inEdgeNode = new EdgeNode[this.G.size()];

        Set<Integer> nodeSet = this.G.getNodeSet();
        this.roots = new LinkedList<>(nodeSet);
        this.edgeNodeCycle = new ArrayList<>(this.G.size());

        HashMap<EdgeNode, PairingHeap<EdgeNode>.Node> tNodeMap = new HashMap<>();
        for (Integer node: nodeSet) {
            this.queues[node] = new PairingHeap<>(this.maxDisjointCmp, tNodeMap);
            this.edgeNodeCycle.add(node, null);
        }

        this.maxEdgeCycle = new WeightedEdge[this.G.size()];
        this.maxCycleEdgeWeights = new Number[this.G.size()];
        this.contractedEdges = new List[this.G.size()];

        this.cameriniForest = aTree;

        this.recoverContraction();
        this.setRoots(aTreeRoots);

        EdgeNode n = aTree.getNode(removeEdge);
        for (EdgeNode c: n.getChildren()) {
            c.setInitialParent(null);
            c.setParent(null);
        }

        aTree.removeFromATNodeLst(removeEdge);

        int i = 0;
        //initialize priority queues
        for (EdgeNode node: removedContracted) {
            ATNode atNode = (ATNode) node;
            Set<ATNode> contractedEdgesSet = atNode.getContractedEdges();
            for (ATNode edgeNode: contractedEdgesSet) {
                WeightedEdge edge = edgeNode.getWeightedEdge();
                int dst = this.stronglyConnected.findSet(edge.getDest());
                this.queues[dst].push(new ATNode(edge,i));
                i++;
            }
        }

        // addEdge case
        if (newEdge != null) {
            int dst = this.stronglyConnected.findSet(newEdge.getDest());
            this.queues[dst].push(new ATNode(newEdge));
        }
    }

    @Override
    protected void populateHeaps() {
        // perform the heap population in th constructor :)
    }

    private void recoverContraction() {

        this.cameriniForest.resetMaxArr();
        this.cameriniForest.clearRset();

        ATree aTree = (ATree) this.cameriniForest;
        List<EdgeNode> roots = aTree.roots();

        for (EdgeNode root: roots){
            Deque<EdgeNode> nodeStack = aTree.reverseOrder(root);
            while (!nodeStack.isEmpty()) {
                EdgeNode node = nodeStack.pop();
                List<EdgeNode> children = node.getChildren();
                this.processATNode(children, (ATNode)node);
            }
        }
    }

    /**
     * Initialize data-structures related with the cycles
     * @param children - List of ATNodes
     */
    private void processATNode(List<EdgeNode> children, ATNode node) {

        WeightedEdge maxEdge = node.getMaxEdgeCycle();
        int maxDst = this.stronglyConnected.findSet(maxEdge.getDest());
        Number reducedCost = node.getMaxCycleW();

        for (EdgeNode child: children) {
            WeightedEdge e = child.getWeightedEdge();
            int dst = this.stronglyConnected.findSet(e.getDest());
            this.inEdgeNode[dst] = this.cameriniForest.getNode(e);
            this.computeReducedCostUtil(dst, reducedCost, e);
        }

        for (EdgeNode child: children) {
            WeightedEdge e = child.getWeightedEdge();
            int src = e.getSrc();
            int dst = e.getDest();
            this.stronglyConnected.unionSet(src, dst);
            this.weaklyConnected.unionSet(src,dst);
        }

        int rep = this.stronglyConnected.findSet(maxEdge.getDest());
        this.cameriniForest.updateMaxArray(rep, maxDst);
        WeightedEdge edge = node.getWeightedEdge();
        EdgeNode edgeNode = this.cameriniForest.getNode(edge);
        List<EdgeNode> edgeNodeChildren = edgeNode.getChildren();
        this.edgeNodeCycle.set(rep, edgeNodeChildren);
        this.maxEdgeCycle[rep] = maxEdge;
        this.contractedEdges[rep] = new ArrayList<>(node.getContractedEdges());
        this.maxCycleEdgeWeights[rep] = reducedCost;
        this.maxEdgeCycle[rep] = maxEdge;
    }

    /**
     * Set V' vertex set to the roots
     * @param aTreeRoots - List of ATNodes that are the roots of the decomposed tree
     */
    private void setRoots(List<EdgeNode> aTreeRoots) {

        this.roots = new LinkedList<>();
        for (EdgeNode node: aTreeRoots) {
            WeightedEdge edge = node.getWeightedEdge();
            int v = this.stronglyConnected.findSet(edge.getDest());
            this.roots.add(v);
        }
    }

    /**
     * Compute reduced costs
     * @param vertex - vertex
     * @param maxW - max weight of cycle
     * @param edge - max cycle edge
     */
    private void computeReducedCostUtil(int vertex, Number maxW, WeightedEdge edge) {
        Number incidentW = this.getAdjustedWeight(edge);
        Number reducedCost = maxW.floatValue() - incidentW.floatValue();
        this.stronglyConnected.addWeight(vertex, reducedCost);
    }

    @Override
    protected EdgeNode getMinEdgeNode(int root) {

        // evaluate next root
        Heap<EdgeNode> pq  = this.queues[root];

        // current component does not have any incident edge
        if (pq.isEmpty()) {
            ATNode node = new ATNode(null);
            boolean hasCycle = ATCycle(node, root);
            if (hasCycle) {
                this.cameriniForest.addEntryToRset(root);
                this.cameriniForest.add(node);
            }
            return null;
        }

        // verifying if current component has any other edge than a self-loop edge
        EdgeNode minEdgeNode = pq.pop();
        WeightedEdge min = minEdgeNode.getWeightedEdge();
        while (!pq.isEmpty() && this.stronglyConnected.sameSet(min.getSrc(), min.getDest())) {
            minEdgeNode = pq.pop();
            min = minEdgeNode.getWeightedEdge();
        }

        int u = min.getSrc();
        int v = min.getDest();

        // this component does not have any edge that is not a self loop so we must check another component
        if (this.stronglyConnected.sameSet(u,v)) {
            ATNode node = new ATNode(null);
            boolean hasCycle = ATCycle(node, root);
            if (hasCycle) {
                this.cameriniForest.addEntryToRset(root);
                this.cameriniForest.add(node);
            }
            return null;
        }

        return minEdgeNode;
    }

    @Override
    protected void contract(int u, int v, int root, EdgeNode minEdgeNode) {
        // perform contraction
        // store nodes in the cycle
        List<Integer> contractionSet = new ArrayList<>();
        contractionSet.add(this.stronglyConnected.findSet(v));

        // keep track of the edges in the cycle
        List<EdgeNode> edgeNodesInCycle = new ArrayList<>();
        edgeNodesInCycle.add(minEdgeNode);

        // map the edge incident in a node
        Map<Integer, EdgeNode> map = new HashMap<>();
        map.put(this.stronglyConnected.findSet(v), minEdgeNode);

        // since a cycle as arisen we need to choose a new minimum weight edge incident in node root
        this.inEdgeNode[root] = null;

        for (int i = this.stronglyConnected.findSet(u);
             this.inEdgeNode[i] != null;
             i = this.stronglyConnected.findSet(this.inEdgeNode[i].getWeightedEdge().getSrc()))
        {

            map.put(i, this.inEdgeNode[i]);
            edgeNodesInCycle.add(this.inEdgeNode[i]);
            contractionSet.add(i);
        }

        // find maximum adjustedWeights edge
        EdgeNode maxEdgeNodeCycle = Collections.max(edgeNodesInCycle, this.maxDisjointCmp);
        WeightedEdge maxEdgeCycle = maxEdgeNodeCycle.getWeightedEdge();

        int dst = stronglyConnected.findSet(maxEdgeCycle.getDest());
        Number maxW = this.getAdjustedWeight(maxEdgeCycle);
        this.computeReducedCost(contractionSet, map, maxW);

        // perform unionSet in the strongly connected
        this.performUnionInStronglyConnected(edgeNodesInCycle);

        // find cycle representative
        int rep = this.stronglyConnected.findSet(maxEdgeCycle.getDest());

        // the representative will be the next node to be evaluated
        this.roots.add(0, rep);

        // unite the heaps
        this.performHeapUnion(rep, contractionSet);

        this.cameriniForest.updateMaxArray(rep, dst);
        this.edgeNodeCycle.set(rep, edgeNodesInCycle);
        this.maxCycleEdgeWeights[rep] = maxW;
        this.maxEdgeCycle[rep] = maxEdgeCycle;
    }

    private boolean ATCycle(ATNode minATNode, int root) {

        List<EdgeNode> cycle = this.edgeNodeCycle.get(root);

        if (cycle == null) {
            return false;
        }

        // set super-node true
        minATNode.setSuperNode(true);
        // set parent to be a root
        minATNode.setParent(null);

        for (EdgeNode edgeNode: cycle) {
            // set parent of cycle node
            edgeNode.setParent(minATNode);
            // make node of cycle a child of the super-node
            minATNode.addChild(edgeNode);
            // add edge to the contracted edge list
            minATNode.addToContractedEdges((ATNode) edgeNode);
        }

        // add the contracted edges to the super-node
        List<ATNode> contracted = this.contractedEdges[root];
        if (contracted != null && !contracted.isEmpty()) {
            minATNode.addToContractedEdges(contracted);
        }

        // whenever a node is super-node store the maximum weight edge and its weight
        if (minATNode.getMaxEdgeCycle() == null) {
            minATNode.setMaxEdgeCycle(this.maxEdgeCycle[root]);
            minATNode.setMaxCycleW(this.maxCycleEdgeWeights[root]);
        }

        return true;
    }

    @Override
    protected EdgeNode processCameriniForest(EdgeNode minEdgeNode, int root) {

        minEdgeNode = this.cameriniForest.add(minEdgeNode);

        // if a node was in the tree and is the representative of a cycle no process
        if (!minEdgeNode.getChildren().isEmpty()){
            return minEdgeNode;
        }

        ATNode minATNode = (ATNode) minEdgeNode;
        // set edge and the constant to be added to the edge in ATNode
        minATNode.setCurrentWeight(this.getAdjustedWeight(minEdgeNode.getWeightedEdge()));
        minATNode.setMaxCycleW(this.maxCycleEdgeWeights[root]);
        minATNode.setMaxEdgeCycle(this.maxEdgeCycle[root]);

        // addition of edge does not make a cycle therefore is a leaf in the camerini forest
        if (this.edgeNodeCycle.get(root) == null) {
            this.cameriniForest.addPi(root, minATNode);
        }
        else {
            // cycle arises
            this.ATCycle(minATNode,root);
        }

        return minATNode;
    }

    public ATree getATree() {
        return (ATree) cameriniForest;
    }

    /**
     * Util function that performs the heap union during a contraction
     * @param rep - representative node
     * @param contractionSet - nodes involved in the contraction
     */
    @Override
    protected void performHeapUnion(int rep, List<Integer> contractionSet) {

        Heap<EdgeNode> heap = this.queues[rep];
        List<ATNode> allContracted = new LinkedList<>();
        List<ATNode> contracted = this.findContractedEdges(heap, allContracted);

        for (EdgeNode edge: contracted){
            heap.delete(edge);
        }

        for (Integer node: contractionSet) {
            if (rep != node) {
                Heap<EdgeNode> nodeHeap = this.queues[node];
                contracted = this.findContractedEdges(nodeHeap, allContracted);
                for (EdgeNode edge: contracted){
                    nodeHeap.delete(edge);
                }
                heap.union(nodeHeap);
            }
        }

        this.contractedEdges[rep] = allContracted;
    }

    private List<ATNode> findContractedEdges(Heap<EdgeNode> heap, List<ATNode> allContracted) {

        List<ATNode> contracted = new LinkedList<>();

        for (EdgeNode edgeNode: heap.getKeySet()){

            WeightedEdge edge = edgeNode.getWeightedEdge();
            int src = edge.getSrc();
            int dst = edge.getDest();

            if (this.stronglyConnected.sameSet(src,dst)){
                contracted.add((ATNode) edgeNode);
                allContracted.add((ATNode)edgeNode);
            }
        }

        return contracted;
    }

}
