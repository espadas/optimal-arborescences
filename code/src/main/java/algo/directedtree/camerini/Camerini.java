package algo.directedtree.camerini;

import algo.directedtree.DirectedTree;
import datastructures.disjointsets.DirectedMSTDisjointSet;
import datastructures.disjointsets.DisjointSet;
import datastructures.disjointsets.FasterDirectedMSTDisjointSet;
import datastructures.graph.DirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import datastructures.heaps.Heap;
import datastructures.heaps.pairing.PairingHeap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Camerini implements DirectedTree {

    protected DirectedGraph G;
    protected Comparator<WeightedEdge> cmp;
    protected Comparator<EdgeNode> maxDisjointCmp;
    protected Heap<EdgeNode>[] queues;

    protected DisjointSet weaklyConnected;
    protected DirectedMSTDisjointSet stronglyConnected;

    protected LinkedList<Integer> roots;
    protected CameriniForest cameriniForest;
    protected EdgeNode[] inEdgeNode;

    protected List<List<EdgeNode>> edgeNodeCycle;

    protected Camerini() {

    }

    public Camerini(DirectedGraph G, Comparator<WeightedEdge> comparator) {

        this.G = new DirectedGraph(G);
        this.G.reverseGraph();

        this.cmp = comparator;

        this.maxDisjointCmp = (o1, o2) -> {
            WeightedEdge e1 = getAdjustedWeightedEdge(o1.getWeightedEdge());
            WeightedEdge e2 = getAdjustedWeightedEdge(o2.getWeightedEdge());
            return cmp.compare(e1, e2);
        };

        this.stronglyConnected = new FasterDirectedMSTDisjointSet(this.G.size());
        this.weaklyConnected = new DisjointSet(this.G.size());
        this.queues = new Heap[this.G.size()];
        this.inEdgeNode = new EdgeNode[this.G.size()];

        this.cameriniForest = new CameriniForest(this.G.size(), this.G.edges());

        Set<Integer> nodeSet = this.G.getNodeSet();
        this.roots = new LinkedList<>(nodeSet);
        this.edgeNodeCycle = new ArrayList<>(this.G.size());

        HashMap<EdgeNode,PairingHeap<EdgeNode>.Node> tNodeMap = new HashMap<>();
        for (Integer node: nodeSet) {
            this.queues[node] = new PairingHeap<>(this.maxDisjointCmp, tNodeMap);
            this.edgeNodeCycle.add(node, null);
        }

        this.populateHeaps();
    }

    protected void populateHeaps() {
        List<WeightedEdge> allEdges = this.G.getAllEdges();
        for (WeightedEdge edge: allEdges) {
            this.queues[edge.getDest()].push(new EdgeNode(edge));
        }
    }

    @Override
    public List<WeightedEdge> getDirectedTree() {

        while (!this.roots.isEmpty()) {

            int root = this.roots.pop();

            EdgeNode minEdgeNode = this.getMinEdgeNode(root);

            if (minEdgeNode == null) {
                continue;
            }

            minEdgeNode = this.processCameriniForest(minEdgeNode, root);
            WeightedEdge minEdge = minEdgeNode.getWeightedEdge();

            int u = minEdge.getSrc();
            int v = minEdge.getDest();

            // safe to add edge to the arborescense
            if (this.weaklyConnected.findSet(u) != this.weaklyConnected.findSet(v)){
                this.safeAddition(u, v, root, minEdgeNode);
            }
            else {
                // perform a contraction
                this.contract(u, v, root, minEdgeNode);
            }
        }

        // return solution
        return this.cameriniForest.expansion();
    }

    @Override
    public Number computeWeight(List<WeightedEdge> edges) {
        Number result = 0f;
        for (WeightedEdge edge: edges)
            result = result.floatValue() + edge.getWeight().floatValue();
        return result;
    }

    private void safeAddition(int u, int v, int root, EdgeNode minEdgeNode){
        this.inEdgeNode[root] = minEdgeNode;
        this.weaklyConnected.unionSet(u, v);
    }

    protected void contract(int u, int v, int root, EdgeNode minEdgeNode) {
        // perform contraction
        // store nodes in the cycle
        List<Integer> contractionSet = new ArrayList<>();
        contractionSet.add(this.stronglyConnected.findSet(v));

        // keep track of the edges in the cycle
        List<EdgeNode> edgeNodesInCycle = new ArrayList<>();
        edgeNodesInCycle.add(minEdgeNode);

        // map the edge incident in a node
        Map<Integer, EdgeNode> map = new HashMap<>();
        map.put(this.stronglyConnected.findSet(v), minEdgeNode);

        // since a cycle as arisen we need to choose a new minimum weight edge incident in node root
        this.inEdgeNode[root] = null;

        for (int i = this.stronglyConnected.findSet(u);
             this.inEdgeNode[i] != null;
             i = this.stronglyConnected.findSet(this.inEdgeNode[i].getWeightedEdge().getSrc()))
        {

            map.put(i, this.inEdgeNode[i]);
            edgeNodesInCycle.add(this.inEdgeNode[i]);
            contractionSet.add(i);
        }

        // find maximum adjustedWeights edge
        EdgeNode maxEdgeNodeCycle = Collections.max(edgeNodesInCycle, this.maxDisjointCmp);
        WeightedEdge maxEdgeCycle = maxEdgeNodeCycle.getWeightedEdge();

        int dst = stronglyConnected.findSet(maxEdgeCycle.getDest());
        Number maxW = this.getAdjustedWeight(maxEdgeCycle);
        this.computeReducedCost(contractionSet, map, maxW);

        // perform unionSet in the strongly connected
        this.performUnionInStronglyConnected(edgeNodesInCycle);

        // find cycle representative
        int rep = this.stronglyConnected.findSet(maxEdgeCycle.getDest());

        // the representative will be the next node to be evaluated
        this.roots.add(0, rep);

        // unite the heaps
        this.performHeapUnion(rep, contractionSet);

        this.cameriniForest.updateMaxArray(rep, dst);
        this.edgeNodeCycle.set(rep, edgeNodesInCycle);
    }

    /**
     * Util function that computes the reduced costs
     * @param contractionSet - nodes in the cycle
     * @param map - Map that given a node id returns the edge incident in the node
     * @param maxW - Max weight of a cycle edge
     */
    protected void computeReducedCost(List<Integer> contractionSet, Map<Integer, EdgeNode> map, Number maxW) {
        for (Integer node: contractionSet) {
            Number incidentW = this.getAdjustedWeight(map.get(node).getWeightedEdge());
            Number reducedCost = maxW.floatValue() - incidentW.floatValue();
            this.stronglyConnected.addWeight(node, reducedCost);
        }
    }

    /**
     * Util function that unites all the nodes involved in the cycle
     * @param edgeNodeCycle - Edgenode in the cycle
     */
    protected void performUnionInStronglyConnected(List<EdgeNode> edgeNodeCycle) {
        // perform unionSet in the strongly connected
        for (EdgeNode edgeNode : edgeNodeCycle) {
            WeightedEdge weightedEdge = edgeNode.getWeightedEdge();
            this.stronglyConnected.unionSet(weightedEdge.getSrc(), weightedEdge.getDest());
        }
    }

    /**
     * Util function that performs the heap union during a contraction
     * @param rep - representative node
     * @param contractionSet - nodes involved in the contraction
     */
    protected void performHeapUnion(int rep, List<Integer> contractionSet) {
        Heap<EdgeNode> heap = this.queues[rep];
        // perform heap union
        for (Integer node : contractionSet) {
            if (rep != node) {
                heap.union(this.queues[node]);
            }
        }
    }

    /**
     * Find the minimum edge incident in root node
     * @param root - node identifier
     * @return null if PQ[root] is empty or no edge in PQ[root]
     */
    protected EdgeNode getMinEdgeNode(int root) {

        // evaluate next root
        Heap<EdgeNode> pq = this.queues[root];

        // current component does not have any incident edge
        if (pq.isEmpty()) {
            this.cameriniForest.addEntryToRset(root);
            return null;
        }

        // verifying if current component has any other edge than a self-loop edge
        EdgeNode minEdgeNode = pq.pop();
        WeightedEdge min = minEdgeNode.getWeightedEdge();
        while (!pq.isEmpty() && this.stronglyConnected.sameSet(min.getSrc(), min.getDest())) {
            minEdgeNode = pq.pop();
            min = minEdgeNode.getWeightedEdge();
        }

        int u = min.getSrc();
        int v = min.getDest();

        // this component does not have any edge that is not a self loop so we must check another component
        if (this.stronglyConnected.sameSet(u,v)) {
            this.cameriniForest.addEntryToRset(root);
            return null;
        }

        return minEdgeNode;
    }

    /**
     * Util function that adds the minEdgeNode as a leaf or if a cycle emerges as the parent of all Edge nodes involved
     * in the cycle
     * @param minEdgeNode - min edge node
     * @param root - node being evaluated
     */
    protected EdgeNode processCameriniForest(EdgeNode minEdgeNode, int root) {

        minEdgeNode = this.cameriniForest.add(minEdgeNode);

        // addition of edge does not make a cycle therefore is a leaf in the camerini forest
        if (this.edgeNodeCycle.get(root) == null) {
            this.cameriniForest.addPi(root, minEdgeNode);
        }
        else {
            // cycle arises
            for (EdgeNode node: this.edgeNodeCycle.get(root)) {
                node.setParent(minEdgeNode);
                minEdgeNode.addChild(node);
            }
        }

        return minEdgeNode;
    }

    /**
     * Get the adjusted weight of a given edge
     *
     * @param edge - input edge
     * @return new WeightedEdge object with the new
     */
    protected WeightedEdge getAdjustedWeightedEdge(WeightedEdge edge) {
        Number w = this.getAdjustedWeight(edge);
        return new WeightedEdge(edge.getSrc(), edge.getDest(), w);
    }

    /**
     * Calculate the adjusted weight of a given edge
     *
     * @param edge - input edge
     * @return new weight
     */
    protected Number getAdjustedWeight(WeightedEdge edge) {
        return edge.getOriginalWeight().floatValue() + this.stronglyConnected.findWeight(edge.getDest()).floatValue();
    }

    public CameriniForest getCameriniForest() {
        return cameriniForest;
    }
}
