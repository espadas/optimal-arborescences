package cmd;

import algo.directedtree.DirectedTree;
import algo.directedtree.camerini.Camerini;
import comparators.Comparators;
import datastructures.graph.DirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import io.IO;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean; 
import java.lang.management.MemoryType; 

public class StaticCmd {

    private static final Logger LOGGER = Logger.getLogger(StaticCmd.class.getName());

    public static void main(String[] args) throws IOException {
        run(args);
    }

    /**
     * Run method
     * @param args - cmd arguments
     * @throws IOException - When the input or output file is not found
     */
    private static void run(String[] args) throws IOException {

        long startTime = System.currentTimeMillis();

        Options options = createOptions();
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOGGER.log(Level.SEVERE, e.toString());
            formatter.printHelp("Available arguments: ", options);
            System.exit(1);
        }

        String inputFilePath = cmd.getOptionValue("input");
        String outputFilePath = cmd.getOptionValue("output");

        boolean phylogenetic = cmd.hasOption("phylogenetic");
        boolean branchOpt = cmd.hasOption("local branch optimization");

        if (!phylogenetic && branchOpt) {
            LOGGER.log(Level.SEVERE,"-l must be used with -p");
            System.exit(-1);
        }

        DirectedGraph directedGraph;
        Comparator<WeightedEdge> edgeComparator;

        if (phylogenetic) {
            directedGraph = IO.phylogeneticDirectedGraph(inputFilePath);
        } else {
            directedGraph = IO.readDirectedGraph(inputFilePath);
        }

	resetMemoryPool();

        LOGGER.log(Level.INFO, "Time: LOAD: " + (System.currentTimeMillis() - startTime));

        edgeComparator = Comparators.totalOrderCmp();
        DirectedTree directedTree = new Camerini(directedGraph, edgeComparator);

        LOGGER.log(Level.INFO, "Time: INIT: " + (System.currentTimeMillis() - startTime));

        List<WeightedEdge> weightedEdges = directedTree.getDirectedTree();

        LOGGER.log(Level.INFO, "Time: ALGO: " + (System.currentTimeMillis() - startTime));

        if (branchOpt) {
            // perform branch optimization
            throw new UnsupportedOperationException("Not supported yet.");

            //LOGGER.log(Level.INFO, "Time: LOPT: " + (System.currentTimeMillis() - startTime));
        }

	printMemoryPool();

        // finally write solution
        IO.writeDirectedTreeToFile(weightedEdges, outputFilePath);

        LOGGER.log(Level.INFO, "Time: SAVE: " + (System.currentTimeMillis() - startTime));

    }

    /**
     * Create the cmd options
     * @return Options object
     */
    private static Options createOptions() {

        Options options = new Options();
        Option input = new Option("i", "input", true, "Input file path");
        input.setRequired(true);
        options.addOption(input);

        Option output = new Option("o", "output", true, "Output file");
        output.setRequired(true);
        options.addOption(output);

        Option phylogenetic = new Option("p", "phylogenetic", false, "Parse phylogenetic file");
        options.addOption(phylogenetic);

        Option localBranchOptimization = new Option("l", "local branch optimization", false, "Local branch optimization");
        options.addOption(localBranchOptimization);

        return options;
    }

    private static void printMemoryPool() {

        double mem = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())/1024d;
        LOGGER.info("Mem: END: " + mem + " (KB) -- " + getGcCount());

	double max = 0, sum = 0;
        //LOGGER.info("Mem: " + ManagementFactory.getMemoryMXBean().getHeapMemoryUsage());
        //LOGGER.info("Mem: Non-Heap: " + ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage());
        Iterator<MemoryPoolMXBean> iter = ManagementFactory.getMemoryPoolMXBeans().iterator();
        while (iter.hasNext()) {
            MemoryPoolMXBean item = iter.next();
	    if (item.getType() == MemoryType.HEAP) {
		double val = item.getPeakUsage().getUsed()/1024d;
                LOGGER.info("Peak: " + val + " (KB, " + item.getName() + ": " + item.getPeakUsage() + ")");
		if (val > max) max = val;
		sum += val;
	    }
        }
        LOGGER.info("Peak: MAX: " + max + " (KB)");
        LOGGER.info("Peak: SUM: " + sum + " (KB)");
    }

    private static long getGcCount() {
        long sum = 0;
	for (GarbageCollectorMXBean b : ManagementFactory.getGarbageCollectorMXBeans()) {
            long count = b.getCollectionCount();
            if (count != -1) sum +=  count;
        }
        return sum;
    }

    private static void resetMemoryPool() {
        long before = getGcCount();
	System.gc();
	System.runFinalization();
	while(getGcCount() == before);

        Iterator<MemoryPoolMXBean> iter = ManagementFactory.getMemoryPoolMXBeans().iterator();
        while (iter.hasNext()) {
            MemoryPoolMXBean item = iter.next();
	    item.resetPeakUsage();
        }

        double mem = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())/1024d;
        LOGGER.info("Mem: INI: " + mem + " (KB) -- " + getGcCount());
    }
}

