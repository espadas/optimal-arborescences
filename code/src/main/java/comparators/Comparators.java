package comparators;

import datastructures.graph.edge.WeightedEdge;

import java.util.Comparator;

public class Comparators {

    private Comparators() {}

    /**
     * Simple Edge comparator only compares weights
     *
     * @return Comparator of edges
     */
    public static Comparator<WeightedEdge> getSimpleComparator() {
        Comparator<WeightedEdge> cmp = (o1, o2) -> Float.compare(o1.getWeight().floatValue(), o2.getWeight().floatValue());
        return cmp;
    }

    public static Comparator<WeightedEdge> totalOrderCmp() {

        Comparator<WeightedEdge> cmp = (o1, o2) -> {

            int res = Float.compare(o1.getWeight().floatValue(), o2.getWeight().floatValue());

            if (res != 0) {
                return res;
            }

            return endpointEval(o1, o2);
        };

        return cmp;
    }

    public static int endpointEval(WeightedEdge o1, WeightedEdge o2) {

        int u1 = o1.getSrc();
        int v1 = o1.getDest();

        int u2 = o2.getSrc();
        int v2 = o2.getDest();

        int u = Integer.min(u1, v1);
        int v = Integer.min(u2, v2);

        int r = u - v;

        if (r != 0)
            return r;

        u = Integer.max(u1, v1);
        v = Integer.max(u2, v2);

        r = u - v;

        if (r != 0)
            return r;


        return 0;
    }
}
