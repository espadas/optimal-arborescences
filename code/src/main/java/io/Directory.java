package io;

public class Directory {

    public static String BASE_DIR = "../data/";
    public static String TEST_GRAPH = BASE_DIR + "basic_test/";
    public static String TEST_GRAPH_SOLUTION = BASE_DIR + "basic_test_sol/";

    private Directory() {}
}
